package mommy;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MommyReplaceTest {
    MommyReplace mmReplace = new MommyReplace();
    @Test
    public void shouldReturnSpaceGivenSpace(){
        assertEquals(" ", mmReplace.stringTransform(" "));
    }
    @Test
    public void shouldReturnMommyGivenA(){
        assertEquals("mommy", mmReplace.stringTransform("A"));
    }
    @Test
    public void shouldReturnSkyGivenSky(){
        assertEquals("sky", mmReplace.stringTransform("sky"));
    }
    @Test
    public void shouldReturnBlmommyGivenBla(){
        assertEquals("Blmommy", mmReplace.stringTransform("Bla"));
    }
    @Test
    public void shouldReturnBlehGivenBleh(){
        assertEquals("Bleh", mmReplace.stringTransform("Bleh"));
    }
    @Test
    public void shouldReturnBmommylGivenBaal(){
        assertEquals("Bmommyl", mmReplace.stringTransform("Baal"));
    }
    @Test
    public void shouldReturnBmommykmommylGivenBake(){
        assertEquals("Bmommykmommy", mmReplace.stringTransform("Bake"));
    }
}
