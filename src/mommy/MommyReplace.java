package mommy;

public class MommyReplace {
    public String stringTransform(String input){
        return isVowelsMoreThan30Percentage(input)? replaceVowels(input) : input;
    }
    public String replaceVowels(String input){
        String res = "";
        for (int i = 0; i < input.length(); i++){
            if (isVowel(input.charAt(i))){
                res += res.indexOf("mommy", res.length() - 5) >= 0? "" : "mommy";
            }else {
                res += input.charAt(i);
            }
        }
        return res;
    }
    protected boolean isVowelsMoreThan30Percentage(String input){
        return countVowels(input) > (input.length() * 0.3);
    }
    protected int countVowels(String input){
        int numOfVowels = 0;
        for (int i = 0; i < input.length(); i++){
            if (isVowel(input.charAt(i)))
                numOfVowels++;
        }
        return numOfVowels;
    }
    protected boolean isVowel(char input){
        return "aeiouAEIOU".indexOf(input) >= 0;
    }
}
